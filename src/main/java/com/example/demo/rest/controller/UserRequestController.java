package com.example.demo.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/new-user")
@Api(tags = "User Controller")
public class UserRequestController
{
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Creates a user")
    public int createUser(@Valid @ApiParam(value = "Pool Name") @RequestHeader("pool-name") String poolName,
                          @ApiParam(name = "user", value = "User") @RequestBody final UserRequest userRequest,
                          HttpServletResponse response)
    {
        return 10;
    }


    @ApiOperation("Creates a user")
    @SuppressWarnings("unchecked")
    @DeleteMapping("/{host-name}")
    public int deleteUser(@Valid @ApiParam(value = "Pool Name") @RequestHeader("pool-name") String poolName,
                          @Valid @PathVariable("host-name") final String hostName,
                          @Valid @RequestParam("name") final String name,
                          @Valid @RequestParam final Map extension)

    {
        System.out.println(extension);
        return 10;
    }

}
