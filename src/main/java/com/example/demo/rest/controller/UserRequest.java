package com.example.demo.rest.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("Rajeev")
public class UserRequest
{
     String name;
     int age;
     String gender;
    @JsonProperty(value = "ip-address")
    String ipAddress;

}
