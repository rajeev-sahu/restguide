package com.example.demo.rest.controller;

import com.example.demo.rest.core.User;
import com.example.demo.rest.core.UserService;
import com.example.demo.rest.db.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.text.MessageFormat;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController
{
    @Autowired
    UserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public int createUser(@RequestBody final User user, HttpServletResponse response)
    {
        User createdUser = userService.addUser(user);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().
                path("/{user-id}").buildAndExpand(createdUser.getId()).toUri();

        response.setHeader("Location", uri.toASCIIString());

        return createdUser.getId();
    }

    @GetMapping("/{name}")
    public List<User> getUsers(@PathVariable final String name)
    {
        return userService.getUsers(name);
    }

    @DeleteMapping("/{name}")
    public void deleteUser(@PathVariable final String name)
    {
        System.out.println(MessageFormat.format("Trying to delete {0}", name));
        userService.deleteUsers(name);
    }
}
