package com.example.demo.rest.controller;

import com.example.demo.rest.core.Greeting;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;
import java.util.concurrent.atomic.AtomicLong;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class IntroductionController
{
    private final AtomicLong counter = new AtomicLong();

    @GetMapping(value = "/hello")
    public Greeting sayHello(@RequestParam(value = "name", defaultValue = "Rajeev") final String name)
    {
        final Greeting greeting = new Greeting(counter.getAndIncrement(), MessageFormat.format("Hello, {0}!!",
                                                                                               name));

        greeting.add(ControllerLinkBuilder.linkTo(methodOn(IntroductionController.class).sayHello(name)).withSelfRel());

        return greeting;
    }
}
