package com.example.demo.rest.db;

import com.example.demo.rest.core.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer>
{
    void deleteByName(final String name);
    List<User> findByName(final String name);
}
