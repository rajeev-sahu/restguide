package com.example.demo.rest.core;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Getter
public class User
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    int id;
    String name;
    int age;
    String gender;

    public User(String name, int age, String gender)
    {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
}
