package com.example.demo.rest.core;

import com.example.demo.rest.db.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@Transactional
public class UserServiceImpl implements UserService
{
    @Autowired
    UserRepository userRepository;

    @Override
    public User addUser(final User user)
    {
        return userRepository.save(user);
    }

    @Override
    public List<User> getUsers(final String name)
    {
        return userRepository.findByName(name);
    }

    @Override
    public void deleteUsers(final String name)
    {
        userRepository.deleteByName(name);
//        userRepository.deleteAll(getUsers(name));
    }
}
