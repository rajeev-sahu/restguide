package com.example.demo.rest.core;

import lombok.Getter;
import org.springframework.hateoas.ResourceSupport;

@Getter
public class Greeting extends ResourceSupport
{
    private final long counter;
    private final String name;

    public Greeting(final long counter, final String name)
    {
        this.counter = counter;
        this.name = name;
    }
}
