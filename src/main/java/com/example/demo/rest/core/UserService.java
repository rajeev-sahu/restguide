package com.example.demo.rest.core;

import java.util.List;

public interface UserService
{
    User addUser(final User user);

    List<User> getUsers(final String name);

    void deleteUsers(final String name);
}
